package route

import (
	"github.com/gorilla/mux"
)

func SetRoutes() (*mux.Router, error) {
	r := mux.NewRouter()

	r.HandleFunc("/", ServeHome)

	r.HandleFunc("/flights", APIHandler).Methods("POST")

	return r, nil
}
