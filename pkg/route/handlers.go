package route

import (
	"encoding/xml"
	"fmt"
	"goflights/pkg/config"
	"goflights/pkg/ds"
	"goflights/pkg/lumids/c_log2"
	"goflights/pkg/lumids/c_rs"
	"goflights/pkg/model"
	"log"
	"net/http"
)

var cmLogger = c_log2.GetCMLogger()
var rsSession = c_rs.GetRSSession()
var c = config.GetConfig()

func ServeHome(w http.ResponseWriter, _ *http.Request) {
	_, err := fmt.Fprintf(w, "goflights API home")
	if err != nil {
		log.Printf("error writing homepage message: %s", err)
	}
}

type httpApi interface {
	parseRequest(r *http.Request) error
	execOperation() ([]model.Arrival, error)
	sendResponse([]model.Arrival, http.ResponseWriter, error) error
}

type soapRequest struct {
	XMLName       xml.Name `xml:"Envelope"`
	Text          string   `xml:",chardata"`
	Soap          string   `xml:"soap,attr"`
	EncodingStyle string   `xml:"encodingStyle,attr"`
	Body          struct {
		Text            string `xml:",chardata"`
		M               string `xml:"m,attr"`
		ArrivalsRequest struct {
			Text              string `xml:",chardata"`
			Event             string `xml:"Event"`
			RowID             int    `xml:"RowID"`
			Flightnumber      string `xml:"Flightnumber"`
			Origin            string `xml:"Origin"`
			SchedUnxTimestamp string `xml:"SchedUnxTimestamp"`
			SchedDate         string `xml:"SchedDate"`
			SchedTime         string `xml:"SchedTime"`
		} `xml:"ArrivalsRequest"`
	} `xml:"Body"`
}

type soapResponse struct {
	XMLName       xml.Name `xml:"Envelope"`
	Text          string   `xml:",chardata"`
	Soap          string   `xml:"soap,attr"`
	EncodingStyle string   `xml:"encodingStyle,attr"`
	Body          struct {
		Text              string `xml:",chardata"`
		M                 string `xml:"m,attr"`
		ArrivalssResponse struct {
			Text    string `xml:",chardata"`
			Arrival []model.Arrival
		} `xml:"ArrivalsResponse"`
	} `xml:"Body"`
}

type graphQLRequest struct{} // TODO - Implement GraphQL api type

type restRequest struct{} // TODO - Implement REST api type

func APIHandler(w http.ResponseWriter, r *http.Request) {
	apiType := c.BaseConfig.ApiType

	switch apiType {
	case "REST":
		handleRequest(&restRequest{}, w, r)
	case "SOAP":
		handleRequest(&soapRequest{}, w, r)
	case "GraphQL":
		handleRequest(&graphQLRequest{}, w, r)
	default:
		cmLogger.Message(c_log2.ERR, fmt.Sprintf("Configured API type '%s' is invalid.", apiType))
	}
}

func handleRequest(api httpApi, w http.ResponseWriter, r *http.Request) {
	err := api.parseRequest(r)
	if err != nil {
		cmLogger.Message(c_log2.ERR, fmt.Sprintf("error parsing request: %s", err))
	}

	arrvs, err := api.execOperation()

	if err != nil {
		cmLogger.Message(c_log2.ERR, fmt.Sprintf("error executing api operation: %s", err))
	}

	err = api.sendResponse(arrvs, w, err)
	if err != nil {
		cmLogger.Message(c_log2.ERR, fmt.Sprintf("error sending response to client: %s", err))
	}
}

func (sReq *soapRequest) parseRequest(r *http.Request) error {
	err := xml.NewDecoder(r.Body).Decode(&sReq)
	if err != nil {
		return fmt.Errorf("error decoding xml: %s", err)
	}
	return nil
}

func (sReq *soapRequest) execOperation() ([]model.Arrival, error) {
	switch sReq.Body.ArrivalsRequest.Event {
	case "INSERT":
		err := sReq.insertArrival()
		if err != nil {
			return []model.Arrival{}, fmt.Errorf("error processing soap 'insert' event: %s", err)
		}
	case "GET":
		// TODO - What to return when a specific id doesn't exist?
		arrvs, err := sReq.selectArrivals()
		if err != nil {
			return []model.Arrival{}, fmt.Errorf("error processing soap 'get' event: %s", err)
		}
		return arrvs, nil
	case "UPDATE":
		err := sReq.updateArrival()
		if err != nil {
			return []model.Arrival{}, fmt.Errorf("error processing soap 'update' event: %s", err)
		}
	case "DELETE":
		err := sReq.deleteArrival()
		if err != nil {
			return []model.Arrival{}, fmt.Errorf("error processing soap 'delete' event: %s", err)
		}
	default:
		return []model.Arrival{}, fmt.Errorf("bad soap request, provide valid event type")
	}

	return []model.Arrival{}, nil
}

func (sReq *soapRequest) buildResponse(arrvs []model.Arrival) soapResponse {
	var sResp soapResponse

	sResp.Body.ArrivalssResponse.Arrival = arrvs

	sResp.XMLName = sReq.XMLName
	sResp.Text = sReq.Text
	sResp.Soap = sReq.Soap
	sResp.EncodingStyle = sReq.EncodingStyle
	sResp.Body.M = sReq.Body.M

	return sResp
}

func (sReq *soapRequest) sendResponse(arrvs []model.Arrival, w http.ResponseWriter, eventErr error) error {
	if len(arrvs) == 0 && eventErr == nil {
		w.WriteHeader(http.StatusOK)
		return nil
	}

	soapResp := sReq.buildResponse(arrvs)

	if eventErr != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, err := fmt.Fprintf(w, fmt.Sprintf("%s", eventErr))
		if err != nil {
			cmLogger.Message(c_log2.ERR, fmt.Sprintf("error writing error message to client: %s", err))
		}
		return nil
	} else {
		w.WriteHeader(http.StatusOK)
	}

	err := xml.NewEncoder(w).Encode(&soapResp)
	if err != nil {
		cmLogger.Message(c_log2.ERR, fmt.Sprintf("error sending response to client: %s", err))
	}

	return nil
}

func (sReq *soapRequest) insertArrival() error {
	var a = model.Arrival{
		Flightnumber:      sReq.Body.ArrivalsRequest.Flightnumber,
		Origin:            sReq.Body.ArrivalsRequest.Origin,
		SchedUnxTimestamp: sReq.Body.ArrivalsRequest.SchedUnxTimestamp,
		SchedDate:         sReq.Body.ArrivalsRequest.SchedDate,
		SchedTime:         sReq.Body.ArrivalsRequest.SchedTime,
	}

	var err error
	a.RowID, err = rsSession.NextRowID("ARRF")
	if err != nil {
		return fmt.Errorf("error getting next row id for table 'ARRF': %s", err)
	}

	err = model.InsertArrival(a)
	if err != nil {
		return fmt.Errorf("error inserting arrival into db: %s", err)
	}

	err = ds.SendArrivalCreate(a)
	if err != nil {
		return fmt.Errorf("error sending arrival 'insert' with ds: %w", err)
	}
	return nil
}

func (sReq *soapRequest) selectArrivals() ([]model.Arrival, error) {
	var a model.Arrival
	var arrvs []model.Arrival

	a.RowID = sReq.Body.ArrivalsRequest.RowID
	if a.RowID == 0 {
		var err error
		arrvs, err = model.SelectArrivals()
		if err != nil {
			cmLogger.Message(c_log2.ERR, fmt.Sprintf("error getting arrvs from db: %s", err))
			return []model.Arrival{}, err
		}

	} else {
		a, err := model.SelectArrival(a)
		if err != nil {
			cmLogger.Message(c_log2.ERR, fmt.Sprintf("error getting arrival from db: %s", err))
			return []model.Arrival{}, err
		}
		arrvs = []model.Arrival{a}
	}

	return arrvs, nil
}

func (sReq *soapRequest) updateArrival() error {
	var a = model.Arrival{
		RowID:        sReq.Body.ArrivalsRequest.RowID,
		Flightnumber: sReq.Body.ArrivalsRequest.Flightnumber,
		Origin:       sReq.Body.ArrivalsRequest.Origin,
	}

	err := model.UpdateArrival(a)
	if err != nil {
		return fmt.Errorf("error updating arrival in db: %s", err)
	}

	err = ds.SendArrivalUpdate(a)
	if err != nil {
		return fmt.Errorf("error sending arrival 'update' with ds: %w", err)
	}
	return nil
}

func (sReq *soapRequest) deleteArrival() error {
	var a = model.Arrival{
		RowID: sReq.Body.ArrivalsRequest.RowID,
	}

	err := model.DeleteArrival(a)
	if err != nil {
		return fmt.Errorf("error deleting arrival from db: %s", err)
	}

	err = ds.SendArrivalDelete(a)
	if err != nil {
		return fmt.Errorf("error sending arrival 'delete' with ds: %w", err)
	}
	return nil
}

// TODO - Implement GraphQL api type
func (g *graphQLRequest) parseRequest(r *http.Request) error {
	panic("implement me")
}

func (g *graphQLRequest) execOperation() ([]model.Arrival, error) {
	panic("implement me")
}

func (g *graphQLRequest) sendResponse(arrvs []model.Arrival, writer http.ResponseWriter, err error) error {
	panic("implement me")
}

// TODO - Implement REST api type
func (rr *restRequest) parseRequest(r *http.Request) error {
	panic("implement me")
}

func (rr *restRequest) execOperation() ([]model.Arrival, error) {
	panic("implement me")
}

func (rr *restRequest) sendResponse(arrvs []model.Arrival, writer http.ResponseWriter, err error) error {
	panic("implement me")
}
