package ds

import (
	"fmt"
	"goflights/pkg/lumids/c_ds"
	"goflights/pkg/model"
	"strconv"
)

var dsSession = c_ds.GetDSSession()

func SendArrivalCreate(a model.Arrival) error {
	tableId, err := model.GetTableId("ARRF")
	if err != nil {
		return fmt.Errorf("error getting table id for table 'ARRF': %w", err)
	}

	var fields = map[int]string{
		101: strconv.Itoa(a.RowID),
		65:  a.Flightnumber,
		106: a.SchedUnxTimestamp,
		117: a.SchedTime,
		105: a.SchedDate,
	}

	err = dsSession.SendRowCreate(tableId, a.RowID, fields)
	if err != nil {
		return fmt.Errorf("error sending create message of table id '%d' row id '%d' with ds: %w", tableId, a.RowID, err)
	}
	return nil
}

func SendArrivalUpdate(a model.Arrival) error {
	tableId, err := model.GetTableId("ARRF")
	if err != nil {
		return fmt.Errorf("error getting table id for table 'ARRF': %w", err)
	}

	var fields = map[int]string{
		101: strconv.Itoa(a.RowID),
		65:  a.Flightnumber,
	}

	err = dsSession.SendRowUpdate(tableId, a.RowID, fields)
	if err != nil {
		return fmt.Errorf("error sending create message of table id '%d' row id '%d' with ds: %w", tableId, a.RowID, err)
	}
	return nil
}

func SendArrivalDelete(a model.Arrival) error {
	tableId, err := model.GetTableId("ARRF")
	if err != nil {
		return fmt.Errorf("error getting table id for table 'ARRF': %w", err)
	}

	err = dsSession.SendRowDelete(tableId, a.RowID)
	if err != nil {
		return fmt.Errorf("error sending create message of table id '%d' row id '%d' with ds: %w", tableId, a.RowID, err)
	}

	return nil
}
