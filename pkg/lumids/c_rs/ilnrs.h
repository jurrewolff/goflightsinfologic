/*!----------------------------------------------------------------------------
 * @file ilnrs.h
 * @brief Header file for the Row Server API.
 *
 * This contains the prototypes and constants for programs that use the ILNRS API.
 *
 * The caller must be careful not to permit a callback function on a synchronous
 * socket (one used in a call to 'mx_set_sync') to perform ANY RS API function.
 * This includes the callback function used in rs_init () whenever a RS function
 * call is in progress.
 *
 * @author R. Davis
 * @bug No known bugs.
 *
 * @copyright 2017 through 2018 Infologic Nederland B.V. All rights reserved.
 *
 * @version 1.0.0 Initial version.
 * @version 2.0.0 Implemented new versioning system (#14265).
 * @version 3.0.0 Database table and column name changes.
 * @version 3.0.1 Changed for new logging system.
 * @version 4.0.0 New API naming (#18579)
 *
 *------------------------------------------------------------------------------
 */

#ifndef __ILN_RS_H__
#define __ILN_RS_H__

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

/*------------------------------------------------------------------------------
 * general definitions
 *------------------------------------------------------------------------------
 */

// RS Error return codes

#define RS_INV_TBL              -1                      /*!< Invalid table */
#define RS_NO_CONNECTION        -2                      /*!< No RS connection */
#define RS_INV_BULK_REQ         -3                      /*!< Invalid bulk request */

/*------------------------------------------------------------------------------
 * forward declarations
 *------------------------------------------------------------------------------
 */


/*!----------------------------------------------------------------------------
 * @brief Initialise Row Server connection.
 *
 * It is assumed that the Client has already performed an 'mx_init' and has
 * registered with ILNCM.
 *
 * @see rs_close ()
 *
 * @param usrf user callback function
 * @return error code (zero if OK)
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      rs_init (void (*usrf) (mx_evt_desc_t *));

/*!----------------------------------------------------------------------------
 * @brief Close Row Server connection.
 *
 * @see rs_init ()
 *
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern void     rs_close (void);

/*!----------------------------------------------------------------------------
 * @brief Check Row Server connection.
 *
 * @return true if there is a link with the Row Server, false otherwise
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      rs_link (void);

/*!----------------------------------------------------------------------------
 * @brief Get the next row id to use for the given table name.
 *
 * @param table_name the table name
 * @return the row id to use (< 0 indicates an error)
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      rs_get_row (char *table_name);

/*!----------------------------------------------------------------------------
 * @brief Get the next row id to use for the given table id.
 *
 * @param table_id the table id
 * @return the row id to use (< 0 indicates an error)
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      rs_get_row_by_id (int table_id);

/*!----------------------------------------------------------------------------
 * @brief Get a bulk of row ids to use for the given table name.
 *
 * @param table_name the table name
 * @param nrids the number of row ids to get
 * @return the last row id to use (< 0 indicates an error)
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      rs_get_row_bulk (char *table_name, int nrids);

/*!----------------------------------------------------------------------------
 * @brief Reset all row ids.
 *
 * Reset and re-read the tables and row ids.
 *
 * @return error code (zero if OK)
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      rs_reset (void);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // __ILN_RS_H__
