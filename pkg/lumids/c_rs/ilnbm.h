/*!-----------------------------------------------------------------------------
 * @file  ilnbm.h
 * @brief todo
 *
 * @author R. Davis
 *
 * @copyright 1993 through 2019, Infologic Nederland B.V. All rights reserved.
 *
 * @version 1.0.0 Initial version.
 *
 *------------------------------------------------------------------------------
 */

#ifndef __ILN_BM_H__
#define __ILN_BM_H__

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

/*------------------------------------------------------------------------------
 * general definitions
 *------------------------------------------------------------------------------
 */

#define PMC_PDES 0                              /* Pool descriptors          */
#define PMC_IDES (PMC_PDES+1)                   /* Index descriptors         */
#define PMC_INDX (PMC_IDES+1)                   /* Index blocks              */
#define PMC_PPWR 6                              /* The magic number          */
#define PMC_PMIX (1<<PMC_PPWR)                  /* 2**PMC_PMIX               */
#define PMC_NLEN 16                             /* Maximum name length       */

/*------------------------------------------------------------------------------
 * type definitions
 *------------------------------------------------------------------------------
 */

typedef struct dls_head                         /* Any list header           */
{
    struct dls_head *   next;                   /* Forward link              */
    struct dls_head *   prev;                   /* Backward link             */
    struct dls_desc *   desc;                   /* Descriptor                */
} dlt_head, *dlp_head;

typedef struct dls_desc                         /* Any list descriptor       */
{
    struct dls_head     h;                      /* First/Last elements       */
    int                 ecnt;                   /* Element count             */
} dlt_desc, *dlp_desc;

typedef struct                                  /* Index block               */
{
    int                 shft;                   /* The shift factor          */
    unsigned            maxv;                   /* Maximum value, this block */
    void *              x [PMC_PMIX];           /* Index values              */
} pmt_indx, *pmp_indx;

typedef struct                                  /* Pool descriptor           */
{
    dlt_desc            q;                      /* Blocks in this pool       */
    dlp_head            fblk;                   /* First free block          */
    int                 usid;                   /* User id                   */
    int                 size;                   /* Element size              */
    int                 fcnt;                   /* Free element count        */
    char                name [PMC_NLEN];        /* Its name                  */
} pmt_pdes, *pmp_pdes;

typedef struct                                  /* Index Descriptor          */
{
    pmp_indx            root;                   /* Root index                */
    int                 usid;                   /* User pool id.             */
    char                name [PMC_NLEN];        /* Its name                  */
} pmt_ides, *pmp_ides;

typedef struct                                  /* Memory statistics area    */
{
    int                 nr_pools;               /* Nr of pools ever defined  */
    int                 nr_pools_free;          /* Number of pools free      */
    int                 pool_desc_bytes;        /* Internal pool bytes used  */
    int                 nr_pool_blks;           /* Max nr non-tb pool blk usd*/
    int                 nr_pool_blks_free;      /* Nr. non-tb pool blks free */
    int                 pool_bytes;             /* Non-tb pool bytes used    */
    int                 nr_tractor_blks;        /* Max. nr tractor blks used */
    int                 nr_tractor_blks_free;   /* Nr. tractor blocks free   */
    int                 tractor_bytes;          /* Tractor bytes used        */
    int                 nr_bogy_blks;           /* Max. nr. bogy blocks used */
    int                 nr_bogy_blks_free;      /* Nr. bogy blocks free      */
    int                 bogy_bytes;             /* Bogy bytes used           */
    int                 nr_indexes;             /* Nr of indexes ever defined*/
    int                 nr_indexes_free;        /* Number of indexes free    */
    int                 index_desc_bytes;       /* Internal index bytes used */
    int                 nr_index_blks;          /* Max. nr index blocks used */
    int                 nr_index_blks_free;     /* Nr. of index blocks free  */
    int                 index_bytes;            /* Index bytes used          */
    int                 total_bytes;            /* Total nr of bytes in use  */
} pmt_mem, *pmp_mem;

/*      BM MACRO DEFINITIONS            */

#define dlfirst(d)      (((d)->h.next == (dlp_head) (d)) ? NULL : (d)->h.next)
#define dlhome(e)       ((e)->desc)
#define dllast(d)       (((d)->h.prev == (dlp_head) (d)) ? NULL : (d)->h.prev)
#define dllno(d)        ((d)->ecnt)
#define dllnqe(d,e)     (dllnqb(&(d)->h,e))
#define dllnqf(d,e)     (dllnqa(&(d)->h,e))
#define dlmpty(d)       ((dlp_desc) (d)->h.next == (d)->h.desc)
#define dlnxtc(e)       (((e)->next==(dlp_head) (e)->desc) ? \
                                                (e)->desc->h.next : (e)->next)
#define dlnxtl(e)       (((e)->next==(dlp_head) (e)->desc) ? NULL : (e)->next)
#define dlprvc(e)       (((e)->prev==(dlp_head) (e)->desc) ? \
                                                (e)->desc->h.prev : (e)->prev)
#define dlprvl(e)       (((e)->prev==(dlp_head) (e)->desc) ? NULL : (e)->prev)
#define dlqdin(d)       (d)->h.next=(d)->h.prev=(dlp_head)&(d)->h,(d)->h.desc=(dlp_desc)(d),(d)->ecnt=0
#define dlsqin(d)       {&d.h, &d.h, &d, 0}
#define ilrmob(id,k)    iladob (id, k, NULL)

/*------------------------------------------------------------------------------
 * forward declarations
 *------------------------------------------------------------------------------
 */

/* Enqueue after element     */
extern int      dllnqa (dlp_head r, dlp_head e);

/* Enqueue before element    */
extern int      dllnqb (dlp_head r, dlp_head e);

/* Remove element            */
extern dlp_head dllrem (dlp_head e);

/* Add indexed object        */
extern int      iladob (int id, int k, void *p);

/* Define an index           */
extern int      ildefn (void);

/* Name an index             */
extern int      ilname (int id, char *name);

/* Read next ascending       */
extern void *   ilrdna (int id, int k);

/* Read next descending      */
extern void *   ilrdnd (int id, int k);

/* Read indexed object       */
extern void *   ilrdob (int id, int k);

/* Read sequential           */
extern int      ilrdsq (int id, int (*usrf) (int id, void *e));

/* Release an index          */
extern int      ilrels (int id);


/* Define a pool             */
extern int      pmdefn (int n);

/* Get a buffer              */
extern void *   pmgetb (int id);

/* Get memory statistics     */
extern void     pmmem (pmp_mem curr, pmp_mem diff);

/* Name a pool               */
extern int      pmname (int n, char *name);

/* Release a buffer          */
extern int      pmputb (void *p);

/* Relase a pool             */
extern int      pmrels (int id);

/* Get pool status           */
extern int      pmstat (int id, int (*usrf) (int id, int v, void *e));

/* Trim all pools            */
extern void     pmtrim (void);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // __ILN_BM_H__
