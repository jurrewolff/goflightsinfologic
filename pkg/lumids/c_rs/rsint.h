/*----------------------------------------------------------------------------
 * @file rsint.h
 * @brief Internal header file for Row Server related functions
 *
 * This contains the constants for programs using internal Row Server (RS)
 * structures. File 'ilnrs.h' should be used for normal ILNRS API use.
 *
 * @author R. Davis
 * @bug No known bugs.
 *
 * @copyright 2017 Infologic Nederland B.V. All Rights Reserved.
 *
 *-----------------------------------------------------------------------------
 */
#ifndef __RSINT_H__
#define __RSINT_H__
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
 /*-----------------------------------------------------------------------------
 * general definitions
 *-----------------------------------------------------------------------------
 */
// RS Constants
#define RS_MAX_TBL_LEN      16                              /*!< Maximum length of a table name   */
#define RS_TOUT             10000                           /*!< Timeout for Server reply in API  */
#define RS_CM_SYMBOL        "RowServer"                     /*!< CM ini symbol name for RS        */
#define RS_PORT_PROP        "listen_port"                   /*!< CM ini port property name        */
#define RS_PORT_SECURE_PROP "listen_port_secure"            /*!< CM ini secure port property name */
// RS message types
// Note: These numbers should be small integers so that the Row Server can
//       distinguish between a message type and a table name to remain
//       upwards-compatible with older versions of programs
#define RS_MSGT_GETROW_NAME     1                       /*!< Get row using table name (default)   */
#define RS_MSGT_GETROW_ID       2                       /*!< Get row using table id               */
#define RS_MSGT_GETROW_BULK     3                       /*!< Get a bulk of rows                   */
#define RS_MSGT_RESET           4                       /*!< Reset and re-read row ids            */
// RS Memory Pools and Indexed Lists names
#define RS_MAP_POOL_NM  "Table map pool"
#define RS_ROW_LIST_NM  "Row map list"
// RS RDB database access related constants
// Note: Default table and column names of the 'tables' table, to be used when
//       not available through a CM property
#define RS_DFL_TBLNAME          "DD_TABLES"             /*!< 'Tables' table name      */
#define RS_PROP_TNAME           "tables_table_name"     /*!< CM Property name         */
#define RS_DFL_COL_TBLNAME      "DD_TABLE_NAME"         /*!< Table name column name   */
#define RS_PROP_COL_TNAME       "table_name_column"     /*!< CM Property name         */
#define RS_DFL_COL_TBLNUM       "DD_TABLE_NUMBER"       /*!< Table number column name */
#define RS_PROP_COL_TNUM        "table_number_column"   /*!< CM Property name         */
// Select statement to retrieve the next table name and number from the 'tables'
// table in ascending table number order
#define RS_TBL_SELECT   "select %s, %s from %s where DD_VIRTUAL is NULL or DD_VIRTUAL = 0 order by %s"
// Select statement to retrieve the highest row id from a table name
#define RS_ROW_SELECT   "select ROW_ID from %s order by ROW_ID desc"
 /*-----------------------------------------------------------------------------
 * type definitions
 *-----------------------------------------------------------------------------
 */
 /*----------------------------------------------------------------------------
 * @brief Structure for Table/row id mapping
 *
 *-----------------------------------------------------------------------------
 */
typedef struct
{
    char            table_nm [RS_MAX_TBL_LEN];          /*!< Table name                   */
    int             table_id;                           /*!< Table id, defined in 'tables'*/
    int             row_val;                            /*!< Current highest row id.      */
} rs_map, *rsp_map;
#ifdef __cplusplus
}
#endif // __cplusplus
#endif // __RSINT_H__
