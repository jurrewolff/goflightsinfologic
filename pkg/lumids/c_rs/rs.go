package c_rs

/*
#include "ilnmx.h"
#include "ilnrs.h"

extern void cgo_rs_callback(mx_evt_desc_t);
typedef void (*t_cgo_rs_callback) (mx_evt_desc_t);

extern int link_rs(int rs_res, int timeout);

#cgo CFLAGS: -I${SRCDIR}
#cgo LDFLAGS: -L${SRCDIR}/clib -lcm -llog2 -lrs -ltb++ -lmx
*/
import "C"
import (
	"fmt"
	"goflights/pkg/lumids/c_log2"
	"sync"
	"time"
)

var cmLogger = c_log2.GetCMLogger()

var rsError = map[string]int{
	"invalid table":        int(C.RS_INV_TBL),
	"no rs connection":     int(C.RS_NO_CONNECTION),
	"invalid bulk request": int(C.RS_INV_BULK_REQ),
}

type rsSession struct {
	initialized bool
}

var rsSessionInstance *rsSession
var once sync.Once

func GetRSSession() *rsSession {
	once.Do(func() { rsSessionInstance = &rsSession{} })

	return rsSessionInstance
}

func (s *rsSession) Init() error {
	cb := C.t_cgo_rs_callback(C.cgo_rs_callback)

	rsResult := int(C.rs_init(cb))
	if rsResult != 0 {
		s.initialized = false
		return fmt.Errorf("row server initialization failed with code: %d", rsResult)
	}

	linkResult := C.link_rs(C.int(rsResult), C.int(10000))
	if int(linkResult) != 0 {
		s.initialized = false
		return fmt.Errorf("error linking with rs")
	}

	s.initialized = true
	return nil
}

func (s *rsSession) Close() {
	C.rs_close()
	s.initialized = false
}

func (s *rsSession) NextRowID(tableName string) (int, error) {
	rowID := int(C.rs_get_row(C.CString(tableName)))
	if rowID >= 0 {
		return rowID, nil
	}

	errMsg := "error getting row id"
	for errDetail, errCode := range rsError {
		if errCode == rowID {
			return 0, fmt.Errorf("%s: %s", errMsg, errDetail)
		}
	}

	return 0, fmt.Errorf(errMsg)
}

func (s *rsSession) NextRowIDByTableID(tableID int) (int, error) {
	rowID := int(C.rs_get_row_by_id(C.int(tableID)))
	if rowID >= 0 {
		return rowID, nil
	}

	return 0, fmt.Errorf("getting next row id failed with code: %d", rowID)
}

//export goRSCallback
func goRSCallback(cCBRes C.int) {
	cbRes := int(cCBRes)

	switch cCBRes {
	case C.MX_CB_EVT_ACCEPTED:
		cmLogger.Message(c_log2.INFO, fmt.Sprintf("rs connection accepted: %d", cbRes))
	case C.MX_CB_EVT_DISCON:
		cmLogger.Message(c_log2.ALERT, fmt.Sprintf("rs disconnected, retrying connection"))
		var retries int
		for true { // Keep retrying indefinitely, until reconnected
			retries += 1

			err := rsSessionInstance.Init()
			if err != nil {
				cmLogger.Message(c_log2.ALERT, fmt.Sprintf("rs connection retry number '%d' failed", retries))
			} else {
				break
			}
			time.Sleep(5 * time.Second)
		}

	case C.MX_CB_EVT_RDERR:
		cmLogger.Message(c_log2.ALERT, fmt.Sprintf("read error or exception on rs connection: %d", cbRes))
	default:
		cmLogger.Message(c_log2.ERR, fmt.Sprintf("incorrect event code received from rs: %d", cbRes))
	}
}
