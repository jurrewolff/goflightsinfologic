package c_ds

/*
#include "ilnmx.h"
#include "ilncm.h"
#include "ilnds.h"

extern void cgo_ds_callback(mx_evt_desc_t);
typedef void (*t_cgo_ds_callback) (mx_evt_desc_t);

extern int link_ds(int ds_res, int timeout);

#cgo CFLAGS: -I${SRCDIR}
#cgo LDFLAGS: -L${SRCDIR}/clib -lmx -lcm -lds
*/
import "C"
import (
	"fmt"
	"goflights/pkg/lumids/c_log2"
	"sync"
	"time"
)

var cmLogger = c_log2.GetCMLogger()

type dsSession struct {
	initialized bool
}

var dsSessionInstance *dsSession
var once sync.Once

func GetDSSession() *dsSession {
	once.Do(func() { dsSessionInstance = &dsSession{} })

	return dsSessionInstance
}

func (s *dsSession) Init() error {
	cb := C.t_cgo_ds_callback(C.cgo_ds_callback)

	dsResult := int(C.ds_init(cb))
	if dsResult != 0 {
		s.initialized = false
		return fmt.Errorf("distribute server initialization failed with code: %d", dsResult)
	}

	linkResult := C.link_ds(C.int(dsResult), C.int(10000))
	if int(linkResult) != 0 {
		s.initialized = false
		return fmt.Errorf("error linking with distribute server")
	}
	s.initialized = true
	return nil
}

func (s *dsSession) Close() {
	C.ds_close()
	s.initialized = false
}

// SendRowCreate allows to send updates about newly created table rows. tableId and rowId specify information
// about the new row. The fields parameter contains the fieldId and a data value at the specific row and field.
func (s *dsSession) SendRowCreate(tableId int, rowId int, fields map[int]string) error {
	buf := C.ds_row_create(C.int(tableId), C.int(rowId))

	for fieldId, dataValue := range fields {
		C.ds_add_field(buf, C.int(fieldId), C.short(len(dataValue)), C.CString(dataValue))
	}

	err := transmit(buf)
	if err != nil {
		return fmt.Errorf("error transmitting create message to ds: %w", err)
	}

	return nil
}

func (s *dsSession) SendRowUpdate(tableId int, rowId int, fields map[int]string) error {
	buf := C.ds_row_update(C.int(tableId), C.int(rowId))

	for fieldId, dataValue := range fields {
		C.ds_add_field(buf, C.int(fieldId), C.short(len(dataValue)), C.CString(dataValue))
	}

	err := transmit(buf)
	if err != nil {
		return fmt.Errorf("error transmitting update message to ds: %w", err)
	}
	return nil
}

func (s *dsSession) SendRowDelete(tableId, rowId int) error {
	buf := C.ds_row_delete(C.int(tableId), C.int(rowId))
	err := transmit(buf)
	if err != nil {
		return fmt.Errorf("error transmitting delete message to ds: %w", err)
	}
	return nil
}

func transmit(buf *C.tb_trac_t) error {
	dsRes := int(C.ds_transmit(buf))
	if dsRes != 0 {
		return fmt.Errorf("transmit failed with code: %d", dsRes)
	}
	return nil
}

//export goDSCallback
func goDSCallback(cCBRes C.int) {
	cbRes := int(cCBRes)

	switch cCBRes {
	case C.MX_CB_EVT_ACCEPTED:
		cmLogger.Message(c_log2.INFO, fmt.Sprintf("ds connection accepted: %d", cbRes))
	case C.MX_CB_EVT_DISCON:
		cmLogger.Message(c_log2.ALERT, fmt.Sprintf("ds disconnected, retrying connection"))
		var retries int
		for true { // Keep retrying indefinitely, until reconnected
			retries += 1

			err := dsSessionInstance.Init()
			if err != nil {
				cmLogger.Message(c_log2.ALERT, fmt.Sprintf("ds connection retry number '%d' failed", retries))
			} else {
				break
			}
			time.Sleep(5 * time.Second)
		}

	case C.MX_CB_EVT_RDERR:
		cmLogger.Message(c_log2.ALERT, fmt.Sprintf("read error or exception on ds connection: %d", cbRes))
	default:
		cmLogger.Message(c_log2.ERR, fmt.Sprintf("incorrect event code received from ds: %d", cbRes))
	}
}
