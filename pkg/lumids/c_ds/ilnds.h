/*!----------------------------------------------------------------------------
 * @file ilnds.h
 * @brief Header file for the Distribute Server API.
 *
 * This contains the prototypes and constants for programs that use the ILNDS API.
 *
 * @author R. Davis
 * @bug No known bugs.
 *
 * @copyright 2017 Infologic Nederland B.V. All rights reserved.
 *
 * @version 1.0.0 Initial version.
 * @version 2.0.0 Implemented new Log Server calls.
 * @version 3.0.0 Implemented new versioning system (#14231).
 * @version 3.1.0 Implemented secure sockets
 * @version 3.2.0 Prepared for 64bit / new logging system.
 * @version 4.0.0 New API naming (#18579)
 *
 *------------------------------------------------------------------------------
 */

#ifndef __ILN_DS_H__
#define __ILN_DS_H__

/*------------------------------------------------------------------------------
 * includes
 *------------------------------------------------------------------------------
 */

#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

/*------------------------------------------------------------------------------
 * general definitions
 *------------------------------------------------------------------------------
 */

// DS message id's to insert or remove subscriptions

#define DS_INS_SUBSCR           1                       /*!< New subscription           */
#define DS_DEL_SUBSCR           2                       /*!< Remove subscription        */
#define DS_CREATE               3                       /*!< Insert into DB             */
#define DS_DELETE               4                       /*!< Remove from DB             */
#define DS_UPDATE               5                       /*!< Update DB                  */
#define DS_INS_SUBSCR_NO_OWN    6                       /*!< New sub but no own updates */

#define DS_STARTCOL     0                               /*!< Minimum and maximim 'qcset' value */
#define DS_ENDCOL       511                             /*!< For subscriber columns            */

/*------------------------------------------------------------------------------
 * forward declarations
 *------------------------------------------------------------------------------
 */


/*!----------------------------------------------------------------------------
 * @brief Initialise Distribute Server connection.
 *
 * It is assumed that the Client has already performed an 'mx_init' and has
 * registered with ILNCM.
 *
 * @see ds_close ()
 *
 * @param usrf user callback function
 * @return error code (zero if OK)
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      ds_init (void (*usrf) (mx_evt_desc_t *));

/*!----------------------------------------------------------------------------
 * @brief Close Distribute Server connection.
 *
 * @see ds_init ()
 *
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern void     ds_close (void);

/*!----------------------------------------------------------------------------
 * @brief Check Distribute Server connection.
 *
 * @return true if there is a link with the Distribute Server, false otherwise
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      ds_link (void);

/*!----------------------------------------------------------------------------
 * @brief Build a create data message header for the Client.
 *
 * Note that this function is also used by the DS Server.
 *
 * @param table_id reference to FIDS table
 * @param row_id reference to FIDS table row identifier
 * @return tractor containing the create data message header
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern tb_trac_t * ds_row_create (int table_id, int row_id);

/*!----------------------------------------------------------------------------
 * @brief Build an update data message header for the Client.
 *
 * Note that this function is also used by the DS Server.
 *
 * @param table_id reference to FIDS table
 * @param row_id reference to FIDS table row identifier
 * @return tractor containing the update data message header
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern tb_trac_t * ds_row_update (int table_id, int row_id);

/*!----------------------------------------------------------------------------
 * @brief Build a delete data message for the Client.
 *
 * Note that this function is also used by the DS Server.
 *
 * @param table_id reference to FIDS table
 * @param row_id reference to FIDS table row identifier
 * @return tractor containing the delete data message
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern tb_trac_t * ds_row_delete (int table_id, int row_id);

/*!----------------------------------------------------------------------------
 * @brief Add a data field to a message header for the Client.
 *
 * The Client should have called either ds_row_create (), ds_row_update () or
 * ds_row_delete () before calling this function. Using the tractor returned from
 * those calls, this function should then be called for each of the updated (or
 * new or key) data fields. When all the data fields have been added to the
 * message, the Client should call ds_transmit ().
 *
 * Note that the DS Server also uses this function but will not call
 * ds_transmit (). It will send the message to a Client and will itself release
 * the tractor.
 *
 * @param d tractor containing data
 * @param field_id reference to the data field
 * @param val_len length of field value
 * @param value field value
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern void     ds_add_field (tb_trac_t * d, int field_id, short val_len, char *value);

/*!----------------------------------------------------------------------------
 * @brief Transmit a message to the DS Server.
 *
 * It is assumed that the Client has set up a header and any necessary fields in
 * the tractor.
 *
 * @param d tractor containing DS data - will be released by the API
 * @return error code (zero if OK)
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      ds_transmit (tb_trac_t * d);

/*!----------------------------------------------------------------------------
 * @brief Build a new subscription message for the Client and send it to the server.
 *
 * This function inserts a new subscription.
 *
 * @param id subscription identifier
 * @param table_id reference to table
 * @param f_field_id reference to the first field to subscribe to
 * @param l_field_id reference to the last field to subscribe to
 * @param noownupdates true if the subscription should not return your own updates, otherwise false
 * @return error code (zero if OK)
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      ds_subscribe (char *id, int table_id, int f_field_id, int l_field_id,
                              bool noownupdates);

/*!----------------------------------------------------------------------------
 * @brief Build a delete subscription message for the Client and send it to the Server.
 *
 * @param table_id reference to table
 * @param f_field_id reference to the first field to unsubscribe from
 * @param l_field_id reference to the last field to unsubscribe from
 * @return error code (zero if OK)
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      ds_unsubscribe (int table_id, int f_field_id, int l_field_id);

/*!----------------------------------------------------------------------------
 * @brief Get the next message from the DS Server (if any).
 *
 * This function gets the next message from the DS Server from our incoming
 * message list and extracts the message header.
 *
 * This function may be followed by calls to ds_get_field () to get the data
 * fields (if any). If the Client does not wish to get ALL the data fields, he
 * should call ds_release_mem () to release the tractor.
 *
 * If there is no next message, FALSE is returned and the Client should not call
 * ds_get_field () or ds_release_mem ().
 *
 * @param table_id table ID returned for the update
 * @param row_id ROW ID in the table returned for the update
 * @param action type of the update returned (DS_CREATE/DS_UPDATE/DS_DELETE)
 * @return true if message found, false otherwise
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      ds_get_update (int *table_id, int *row_id, short *action);

/*!----------------------------------------------------------------------------
 * @brief Get the next data field from the current message for the Client.
 *
 * The Client should have called ds_get_update () once before calling this function
 * (perhaps repeatedly). Using the tractor internally returned from that call, this
 * function will retrieve the next data field (if any).
 *
 * When there are no more data fields to retrieve, the tractor will be released
 * automatically and this function will return FALSE. Otherwise, the data field
 * will be returned to the Client and TRUE will be returned. If the Client does
 * not wish to get ALL the data fields (i.e. he does not repeatedly call this
 * function until FALSE is returned), he should call ds_release_mem () to release the
 * tractor internally.
 *
 * @param table_id table ID returned for the update
 * @param row_id ROW ID in the table returned for the update
 * @param action type of the update returned (DS_CREATE/DS_UPDATE/DS_DELETE)
 * @param field_id field ID returned for the data field
 * @param val_len length of the field value returned for the data field
 * @param value field value returned for the data field
 * @return true if a data field was found, false otherwise
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      ds_get_field (int *field_id, short *val_len, void **value);

/*!----------------------------------------------------------------------------
 * @brief Releases the tractor used for getting a packed message from our
 *        internal incoming message list.
 *
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern void     ds_release_mem (void);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // __ILN_DS_H__
