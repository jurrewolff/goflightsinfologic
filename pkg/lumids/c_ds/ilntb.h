/*!-----------------------------------------------------------------------------
 * @file  ilntb.h
 *
 * Header file for programs using TB.
 *
 * @author R. Davis
 *
 * @copyright 1993 through 2018, Infologic Nederland B.V. All rights reserved.
 *
 * @version 1.0.0 Initial version.
 * @version 2.0.0 New API naming (#18579)
 *
 *------------------------------------------------------------------------------
 */

#ifndef __ILN_TB_H__
#define __ILN_TB_H__

/*------------------------------------------------------------------------------
 * includes
 *------------------------------------------------------------------------------
 */

#include "ilnbm.h"

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

/*------------------------------------------------------------------------------
 * general definitions
 *------------------------------------------------------------------------------
 */

#define TB_MAX_LEN      15          /* Max. array length for rvec/wvec */

/*------------------------------------------------------------------------------
 * type definitions
 *------------------------------------------------------------------------------
 */

typedef struct tb_trac
{
    dlt_head  h;                    /* To link these */
    int       cmnd;                 /* Command */
    int       dlen;                 /* Data length */
    int       ownr;                 /* Owner */
    dlt_desc  b;                    /* The bogies */
} tb_trac_t;

/*      TB MACRO DEFINITIONS            */

#define tb_command(t)   ((t)->cmnd)
#define tb_length(t)    ((t)->dlen)
#define tb_owner(t)     ((t)->ownr)

/*------------------------------------------------------------------------------
 * forward declarations
 *------------------------------------------------------------------------------
 */

extern int          tb_set_size (int n);
extern tb_trac_t *  tb_allocate (void);
extern void         tb_free (tb_trac_t * p);
extern void         tb_addl (tb_trac_t * p, int n);
extern void         tb_add (tb_trac_t * p, char *t, int n);
extern void         tb_add_str (tb_trac_t * p, char *t);
extern void         tb_add_tb (tb_trac_t * t, tb_trac_t * f);
extern tb_trac_t *  tb_copy (tb_trac_t * p, int n);
extern tb_trac_t *  tb_copyd (tb_trac_t * p);
extern int          tb_get (tb_trac_t * p, char *t, int n);
extern char *       tb_gets (tb_trac_t * p, int *n);
extern int          tb_getx (tb_trac_t * p, char *t, int n, int ch);
extern void         tb_prefix (tb_trac_t * p, char *t, int n);
extern int          tb_peek (tb_trac_t * p, char *t, int n);
extern int          tb_write_vec (tb_trac_t * p, int *n, void *v, int l);
extern int          tb_read_vec (tb_trac_t * p, int n, void *v, int l);
extern int          tb_add_vec (tb_trac_t * p, int n, void *v);
extern int          tb_status (int (*usrf) (int id, int v, void *e));
extern int          tb_ston (char *s, char *f, tb_trac_t * p);
extern int          tb_ntos (tb_trac_t * p, char *f, char *s);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // __ILN_TB_H__
