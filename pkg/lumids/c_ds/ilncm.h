/*!----------------------------------------------------------------------------
 * @file ilncm.h
 * @brief Header file for the Configuration Manager API.
 *
 * This contains the prototypes and constants for programs that use the ILNCM API.
 *
 * @author R. Davis
 * @bug No known bugs.
 *
 * @copyright 2018 through 2019 Infologic Nederland B.V. All rights reserved.
 *
 * @version 1.0.0 Initial version.
 * @version 2.0.0 Implemented process connection status maintenance.
 * @version 3.0.0 Implemented new versioning system (#14199).
 * @version 3.3.0 Added secure sockets (#17876).
 * @version 3.4.0 Added set log level for the process.
 * @version 3.5.0 Implemented native Java program start (#17483).
 * @version 4.0.0 New API naming (#18579)
 *
 *------------------------------------------------------------------------------
 */

#ifndef __ILN_CM_H__
#define __ILN_CM_H__

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

/*------------------------------------------------------------------------------
 * general definitions
 *------------------------------------------------------------------------------
 */

// Port offset constants (offset from envrionment variable CM_BASE_PORT)

#define CM_BOOT_REQ             0                       /*!< Boot request port */
#define CM_CMD_UDP              1                       /*!< UDP commander port */
#define CM_CMD_TCP              2                       /*!< TCP commander port */
#define CM_CON_PORT             3                       /*!< Satellite connect port */
#define CM_PROC_PORT            4                       /*!< Process connect port */
#define CM_USR_PORT             5                       /*!< User (GUI) connect port */
#define CM_CMD_TCP_SECURE       6                       /*!< TCP secure commander port */
#define CM_USR_PORT_SECURE      7                       /*!< User (GUI) secure connect port */

// Error constants

#define CM_E_NOERROR            0                       /*!< No error; may continue */
#define CM_E_NO_CONNECT         1                       /*!< Cannot connect to CM */
#define CM_E_NO_PROCNAME        2                       /*!< No process name in environment */
#define CM_E_NO_PPID            3                       /*!< No CM pid in environment */
#define CM_E_NO_CM_PORT         4                       /*!< No CM_BASE_PORT environment var */
#define CM_E_MX_ERROR           5                       /*!< General MX error */
#define CM_E_UNKNOWN_SOCKET     6                       /*!< Data from unknown socket, app err */
#define CM_E_UNEXPECTED_EVENT   7                       /*!< Unexpected event, app err */
#define CM_E_SYSTEM_ERROR       8                       /*!< CM system error */
#define CM_E_BOUND              9                       /*!< Process already bound */
#define CM_E_REGISTERED         10                      /*!< Process already registered */
#define CM_E_NOT_REGISTERED     11                      /*!< Process not registered */
#define CM_E_SYS_STDO           12                      /*!< Cannot open stdout in cmsystem */
#define CM_E_SYS_STDE           13                      /*!< Cannot open stderr in cmsystem */
#define CM_E_SYS_FORK           14                      /*!< Could not fork in cmsystem */
#define CM_E_SYS_INT            15                      /*!< cmsystem interrupted by signal */
#define CM_E_MAX                15                      /*!< Max error no */

// Predefined Symbol Properties

#define CM_PROP_TYPE            0                       /*!< Symbol type (see above) */
#define CM_PROP_FILE            1                       /*!< File name */
#define CM_PROP_DIR             2                       /*!< Directory path */
#define CM_PROP_AUTO_RESTART    3                       /*!< Auto-restart flag */
#define CM_PROP_INHIBIT         4                       /*!< Inhibit flag */
#define CM_PROP_HOST1           5                       /*!< Host 1 */
#define CM_PROP_HOST2           6                       /*!< Host 2 */
#define CM_PROP_HOST3           7                       /*!< Host 3 */
#define CM_PROP_HOST4           8                       /*!< Host 4 */
#define CM_PROP_HOST5           9                       /*!< Host 5 */
#define CM_PROP_HOST6           10                      /*!< Host 6 */
#define CM_PROP_HOST7           11                      /*!< Host 7 */
#define CM_PROP_HOST8           12                      /*!< Host 8 */
#define CM_PROP_HOST9           13                      /*!< Host 9 */
#define CM_PROP_HOST10          14                      /*!< Host 10 */
#define CM_PROP_STRING          15                      /*!< Generic string prop */
#define CM_PROP_NUMBER          16                      /*!< Generic numeric */
#define CM_PROP_INSTANCE        17                      /*!< Instances */
#define CM_PROP_AUTO_RESTORE    18                      /*!< Auto-restore flag */
#define CM_PROP_ARG             19                      /*!< Arg - arguments string*/
#define CM_PROP_JVM_OPTIONS     20                      /*!< Jvm options for Java programs */
#define CM_PROP_MAX             20                      /*!< Max prop. no */

// Connection type constants
// Note that these should be the same as those defined in ILNCON++

#define CM_CON_TYPE_UNCHANGED   -1                      /*!< Connection type unchanged */
#define CM_CON_TYPE_TEMPORARY   0                       /*!< Temporary connection */
#define CM_CON_TYPE_DELAYED     1                       /*!< A delayed connection */
#define CM_CON_TYPE_REQUIRED    2                       /*!< Required connection */
#define CM_CON_TYPE_ESSENTIAL   3                       /*!< Essential connection */

// Connection status constants

#define CM_CON_STAT_CONNECTED   0                       /*!< Connected */
#define CM_CON_STAT_CLOSED      1                       /*!< Not connected/closed */
#define CM_CON_STAT_CONNECTING  2                       /*!< Attempting to connect */
#define CM_CON_STAT_LOST        3                       /*!< Connection lost/dropped */

/*------------------------------------------------------------------------------
 * forward declarations
 *------------------------------------------------------------------------------
 */


/*!----------------------------------------------------------------------------
 * @brief Bind to the Configuration Manager.
 *
 * The bind function is similar to the register. It tells CM that the process
 * has started and the caller would like to have a connection with CM. However,
 * CM does not expect the process to be in its run list and will not do any
 * checking on the process. It will however send back general information (e.g.
 * ini file location). The intention of a bind is that a process (typically a
 * short-lived utility) needs to make use of cm services (e.g. cm_get_prop) but
 * is not started under the CM regime. This mechanism may be used for manual
 * start of processes, allbeit that the process name will not be available and
 * must be specified by the caller.
 *
 * @see cm_register ()
 *
 * @param pname process name (must be a null byte terminated string)
 * @param mxres ILNMX error code returned if applicable
 * @return CM error code (zero/CM_E_NOERROR if OK)
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      cm_bind (char *pname, int *mxres);

/*!----------------------------------------------------------------------------
 * @brief Register with the Configuration Manager.
 *
 * Register our program with CM; this function will establish a connection with
 * CM. Once connected the register message is sent to CM and an answer is awaited
 * in-line, i.e. the caller is suspended until a reply is received from CM. The
 * result of the function is a code indicating whether the caller may continue,
 * or must abort:
 *   - CM_E_NOERROR (=0)     No error; may continue
 *   - CM_E_NO_CONNECT       Connection with CM could not be established
 *   - CM_E_NO_PROCNAME      No process name in environment
 *   - CM_E_NO_PPID          No CM pid in environment
 *   - CM_E_NO_CM_PORT       No CM_BASE_PORT environment variable
 *   - CM_E_MX_ERROR         General MX error, mx result code in arg <mxres>
 *   - CM_E_UNKNOWN_SOCKET   We got a message from a non-cm socket; must be
 *                           application error (did not call cm_register first).
 *
 * The function will implicitly retrieve (from environment):
 *   - Process name          If NULL -> error
 *   - PID of CM             If NULL -> error
 *
 * @see cm_bind ()
 *
 * @param version version string (must be null byte terminated)
 * @param mxres ILNMX error code returned if applicable
 * @return CM error code (zero/CM_E_NOERROR if OK)
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      cm_register (char *version, int *mxres);

/*!----------------------------------------------------------------------------
 * @brief Deregister from the Configuration Manager.
 *
 * Deregister from CM; to be called before regular process termination. We do NOT
 * disconnect from CM; CM will do that by itself upon receiving the deregister message.
 *
 * @see cm_cant_run ()
 *
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern void     cm_deregister ();

/*!----------------------------------------------------------------------------
 * @brief Inform the Configuration Manager that the process is unable to run on this host.
 *
 * There may be a situation where only the process itself can determine whether
 * it can run on a host. For example a process requiring the availability of
 * serial links may not be able to run as the port(s) can't be opened. In that
 * case the 'unable' message is sent to CM which will process this in its
 * administration by setting the 'cantrun' flag in the host list for the process.
 *
 * @see cm_deregister ()
 *
 * @param reason the reason string (must be null byte terminated)
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern void     cm_cant_run (char *reason);

/*!----------------------------------------------------------------------------
 * @brief Execute a Configuration Manager process.
 *
 * This API call submits a start request to CM for a process. CM will start the
 * process if the symbol exists and if its type is EXE. Note that the call is
 * asynchronous and will return immediately without error result. If execution
 * fails, this will only be visible in the CM log files.
 *
 * Whereas the manually started functions cannot pass arguments to the started
 * process we do allow this function to pass arguments. The reason for this is
 * that, contrary to manually started processes, processes started via this
 * function may need run-time dependent data which cannot be retrieved via the
 * normal properties.
 *
 * The return error code is that from the sending of the start request by ILNMX
 * (zero if OK) or, if the current process has itself not registered or bound
 * with CM, CM_E_NOT_REGISTERED.
 *
 * @param pname process name (must be a null byte terminated string)
 * @param args number of arguments to be passed
 * @param ... the actual arguments to be passed
 * @return ILNMX error code (zero if OK) or CM_E_NOT_REGISTERED.
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      cm_exec (char *pname, int args, ...);

/*!----------------------------------------------------------------------------
 * @brief Set a callback function for application termination by the Configuration Manager.
 *
 * @param f the callback function to be used
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern void     cm_set_cb (void (*f) (void));

/*!----------------------------------------------------------------------------
 * @brief Set a notification callback function.
 *
 * @param notify_type the notification type (any positive integer)
 * @param f the callback function to be used
 * @since 2.0.0
 *
 *------------------------------------------------------------------------------
 */

extern void     cm_set_notify (int notify_type, void (*f) (void));

/*!----------------------------------------------------------------------------
 * @brief Send a message to the Configuration Manager.
 *
 * Sends a message to CM which will be logged in the CM log file and will be
 * kept in the process administration as the last message received from the
 * process. Useful to keep track of the status of a process, additional termination
 * information, etc. Don't overuse this API function!
 *
 * @param fmt the message string (as printf ())
 * @param ... the arguments for fmt if and as necessary
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern void     cm_message (char *fmt, ...);

/*!----------------------------------------------------------------------------
 * @brief Perform a system call.
 *
 * CM's equivalent for 'system'. The difference is that a stdout and stderr can
 * be specified. 'cm_system' opens these before calling 'system'. Using the
 * 'waitpid' call, completion of the script is waited before returning to the
 * caller. No specific error handling is done (the status returned is always
 * from the shell). If stdout and/or stderr are defined as either an empty string
 * or as NULL, they are interpreted as "/dev/null".
 *
 * @param stdo standard output to be used
 * @param std3 standard error to be used
 * @param script the file to be executed
 * @return error code (zero if OK) - usually a child exit code or a CM error code
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      cm_system (char *stdo, char *stde, char *script);

/*!----------------------------------------------------------------------------
 * @brief Get our process name.
 *
 * Returns our own process name if known, or an empty string. Do not alter the
 * string pointed to by the return pointer.
 *
 * @return pointer to an internal ILNCM string containing the known process name
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern char *   cm_my_name ();

/*!----------------------------------------------------------------------------
 * @brief Get Configuration Manager error code string.
 *
 * Returns a pointer to an error string corresponding to a ILNCM API error code.
 * Do not alter the string pointed to by the return pointer. If n is not an ILNCM
 * API error code, NULL is returned.
 *
 * @param n the ILNCM error code you wish the error string for
 * @return pointer to an internal ILNCM string containing the error string, or NULL
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern char *   cm_get_err_str (int n);

/*!----------------------------------------------------------------------------
 * @brief Get Configuration Manager connection socket.
 *
 * @return the socket on which the CM is connected
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      cm_get_socket ();

/*!----------------------------------------------------------------------------
 * @brief Get the CM ini file property value for a section and property number.
 *
 * If the property does not exist, or the property number is invalid, or the
 * ini file does not exist or is not set for the caller, or the property name is
 * empty, NULL is returned.
 *
 * @param section the section name, or NULL for current process name
 * @param propnum the property number (a CM_PROP_... constant)
 * @param s user supplied character area to hold the property value
 * @return the user supplied character area containing the property value, or NULL
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern char *   cm_get_prop (char *section, int propnum, char *s);

/*!----------------------------------------------------------------------------
 * @brief Get the CM ini file property value for a section and property name.
 *
 * If the property does not exist, or the ini file does not exist or is not set
 * for the caller, or the property name is empty, NULL is returned.
 *
 * @param section the section name, or NULL for current process name
 * @param prop the property name
 * @param s user supplied character area to hold the property value
 * @return the user supplied character area containing the property value, or NULL
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern char *   cm_get_prop_str (char *section, char *prop, char *s);

/*!----------------------------------------------------------------------------
 * @brief Get a CM system port number.
 *
 * Gets a CM port by adding the specified offset to the value defined by the
 * environment variable 'CM_BASE_PORT'. If that does not exist, -1 is returned.
 *
 * @param offset the offset to ise to calculate the final value for the port number
 * @return the CM system port number or -1 if not known or valid
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern int      cm_get_sys_port (int offset);

/*!----------------------------------------------------------------------------
 * @brief Report a connection status to the Configuration Manager.
 *
 * The process can use this function to report a connection status change to the
 * Configuration Manager. The function sends a message to CM with the connection
 * status information from the arguments, and CM will then update (or add) the
 * connection information in the process administration.
 *
 * @param pname the process name for the status change
 * @param type the type of the connection (should be a CM_CON_TYPE_... constant)
 * @param status the new status (should be a CM_CON_STAT_... constant)
 * @param note a note to be used for the new connection or NULL if not needed
 * @since 2.0.0
 *
 *------------------------------------------------------------------------------
 */

extern void     cm_con_status (char *pname, short type, short status, char *note);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // __ILN_CM_H__
