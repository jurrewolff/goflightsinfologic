/*!-----------------------------------------------------------------------------
 * @file  ilnmx.h
 *
 * Header file for programs using MX.
 *
 * @author R. Davis
 *
 * @copyright 1993 through 2020, Infologic Nederland B.V. All rights reserved.
 *
 * @version 1.0.0 Initial version.
 * @version 4.0.0 -
 * @version 5.0.0 -
 * @version 5.1.0 Added build option
 * @version 5.2.0 Added secure sockets.
 * @version 5.3.0 Prepared for 64bit
 * @version 6.0.0 New API naming (#18579)
 *
 *------------------------------------------------------------------------------
 */

#ifndef __ILN_MX_H__
#define __ILN_MX_H__

/*------------------------------------------------------------------------------
 * includes
 *------------------------------------------------------------------------------
 */

#include "ilntb.h"

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

/*------------------------------------------------------------------------------
 * general definitions
 *------------------------------------------------------------------------------
 */

/*      Error numbers (temporary until error package available) */

#define MX_E_NOERR                 0            /* No error (success)        */
#define MX_E_SYS                2000            /* General System error      */
#define MX_E_NINIT              2001            /* MX not initialised        */
#define MX_E_AINIT              2002            /* MX already initialised    */
#define MX_E_BADARG             2003            /* Bad argument              */
#define MX_E_BSOCK              2004            /*  - invalid socket number  */
#define MX_E_BDOMAIN            2005            /*  - invalid domain         */
#define MX_E_BHOST              2006            /*  - unknown host name      */
#define MX_E_BBUFFSZ            2007            /*  - invalid buffer size    */
#define MX_E_BLEN               2008            /*  - invalid length         */
#define MX_E_BEVDESC            2009            /*  - invalid event descr.   */
#define MX_E_BOPT               2010            /*  - invalid socket option  */
#define MX_E_BADDR              2011            /*  - invalid address        */
#define MX_E_BMTXQ              2012            /*  - invalid max. TX Q size */
#define MX_E_CBROKEN            2013            /* Connection broken         */
#define MX_E_MAXSOCKS           2014            /* Too many sockets opened   */
#define MX_E_NOPEN              2015            /* Socket not open           */
#define MX_E_SOCKTYPE           2016            /* Socket type invalid 4 op. */
#define MX_E_SBFULL             2017            /* Send buffers full         */
#define MX_E_SSUSPD             2018            /* Socket (already) suspended*/
#define MX_E_SNSUSPD            2019            /* Socket not suspended      */
#define MX_E_NOTIMPL            2020            /* Not yet implemented       */
#define MX_E_NOTACC             2021            /* Message not accepted      */
#define MX_E_BADMHDR            2022            /* Bad message header recvd. */
#define MX_E_BEVCOMB            2023            /* Impossible event combinatn*/
#define MX_E_ENCFAIL            2024            /* Encryption package fail   */
#define MX_E_NERR               2025            /* Next MX error nr to use.  */

/*      Events from the 'select' call   */

#define MX_EVT_TIMEOUT             0            /* Timeout (No-op)           */
#define MX_EVT_DATA                1            /* Data available on socket  */
#define MX_EVT_DATAGRM             2            /* Datagram avail. on socket */
#define MX_EVT_CONREQ              3            /* Connection request        */
#define MX_EVT_WRITEN              4            /* Write enabled             */
#define MX_EVT_EXCEPT              5            /* Exception condition       */
#define MX_EVT_HANDLED             6            /* Event handled by callback */

/*      Events for the callback functionality   */

#define MX_CB_EVT_MSG             16            /* Data/Datagram msg. read   */
#define MX_CB_EVT_DISCON          32            /* Connection broken by peer */
#define MX_CB_EVT_INCMSG          64            /* Incomplete message        */
#define MX_CB_EVT_RDERR          128            /* Read error/Exception      */
#define MX_CB_EVT_WRITEN         256            /* Write enabled             */
#define MX_CB_EVT_ACCEPTED       512            /* Client connection accepted*/
#define MX_CB_EVT_ACCEPTFAIL    1024            /* Conn. req. failed/Exceptn.*/

/*      Socket status bit values        */

#define MX_SST_OPEN                1            /* Socket is open            */
#define MX_SST_SUSPD               2            /* Socket is supsended       */
#define MX_SST_CLIENT             16            /* Client (for I/O) socket   */
#define MX_SST_SERVER             32            /* Server (for listening)    */
#define MX_SST_TCPIP             128            /* TCP/IP socket             */
#define MX_SST_UDPIP             256            /* TCP/UDP socket            */
#define MX_SST_UNIX              512            /* Unix (single host only)   */
#define MX_SST_XNSSPP           1024            /* XNS/SPP socket            */
#define MX_SST_XNSIDP           2048            /* XNS/IDP socket            */
#define MX_SST_MASK             3968            /* Mask of all protocol doms.*/

/*      Socket options                  */

#define MX_SOP_SNDBF               0            /* Set send buffer size      */
#define MX_SOP_RCVBF               1            /* Set receive buffer size   */
#define MX_SOP_BLK                 2            /* Set blocking on I/O       */
#define MX_SOP_NOBLK               3            /* Set non-blocking on I/O   */
#define MX_SOP_WRENOFF             4            /* Set write enable test off */
#define MX_SOP_RAW                 5            /* Raw mode                  */
#define MX_SOP_QLEN                6            /* Maximum TX queue length   */
#define MX_SOP_NOLINGER            7            /* No linger (default)       */
#define MX_SOP_LINGER              8            /* Linger                    */
#define MX_SOP_SHORT_MSGS          9            /* Set 2-byte message length */
#define MX_SOP_LONG_MSGS          10            /* Set 4-byte message length */
#define MX_SOP_SECURE             11            /* Secure (encrypted) mode   */
#define MX_SOP_TRUSTEDONLY        12            /* Trusted hosts only mode   */
#define MX_SOP_NOTRUSTEDONLY      13            /* All hosts mode            */

/*      Miscellaneous constants         */

#define MX_MAX_SOCKS            1024            /* Max nr sockets per proc.  */
#define MX_NO_EXIT_HDL             0            /* No exit handler flag      */
#define MX_SET_EXIT_HDL            1            /* Install exit handler flag */
#define MX_ALL_SOCKS              -1            /* All sockets constant      */
#define MX_CLOSED_SOCK            -2            /* Closed socket nr constant */
#define MX_TO_NONE                 0            /* No timeout on select      */
#define MX_TO_FOREVER             -1            /* Wait forever on select    */
#define MX_NO_LOC_PORT             0            /* No local port specified   */
#define MX_TCP_BUF_SIZE         1560            /* TCP/IP 1-packet buff size */
#define MX_MAX_BUF_SIZE        52000            /* Maximum snd/rcv buff size */
#define MX_MAX_TXQ             20000            /* Maximum TX Q length       */
#define MX_TR_OWNER            0xFF00           /* Our tractor owner base    */
#define MX_LOCALHOST     "LUMIDS_LOCAL_HOST"    /* Base lcl hst nm env var nm*/
#define MX_TRUSTED_HOSTS "LUMIDS_TRUSTED_HOSTS" /* Trusted hosts env var nmi */
#define MX_MULTICAST     "LUMIDS_MULTICAST"     /* Multicast addrs env var nm*/
#define MX_DFLT_MC_GROUP NULL                   /* Default multicast grp addr*/

/*------------------------------------------------------------------------------
 * type definitions
 *------------------------------------------------------------------------------
 */

typedef struct mx_evt_desc                      /* Event descriptor          */
{
    int   socknr;                               /* Socket number of event    */
    int   event;                                /* The actual event          */
    int   totsocks;                             /* Number of sockets scanned */
} mx_evt_desc_t;

typedef struct mx_addr                          /* Address structure for     */
{                                               /* Read/Write datagram       */
    char  addr [16];                            /* Internet only for now.    */
} mx_addr_t;

/*------------------------------------------------------------------------------
 * forward declarations
 *------------------------------------------------------------------------------
 */

extern int      mx_accept (int sock, int * newsock);
extern int      mx_addr_name (mx_addr_t *addr, char *name);
extern int      mx_addr_str (mx_addr_t *addr, char *host, int *port);
extern int      mx_addr_to_in (mx_addr_t *addr, unsigned int *inaddr);
extern int      mx_connect (mx_addr_t *addr, int domain, int lport, int *sock);
extern int      mx_close (int sock);
extern int      mx_close_all (void);
extern int      mx_event (mx_evt_desc_t *edesc, int timeout);
extern int      mx_exit (void);
extern int      mx_get_addr (char *host, int port, int domain, mx_addr_t *addr);
extern char *   mx_get_err_str (int err);
extern int      mx_get_local (int sock, mx_addr_t *addr);
extern int      mx_get_peer (int sock, mx_addr_t *addr);
extern int      mx_get_sender (mx_addr_t *addr);
extern int      mx_get_port (char *serv);
extern int      mx_htonl (int nr);
extern short    mx_htons (short nr);
extern int      mx_init (int maxsocks, int sndbufsz, int rcvbufsz, int exithndl);
extern int      mx_in_to_addr (unsigned int inaddr, mx_addr_t *addr);
extern int      mx_join_mc (int sock, char *groupaddr);
extern int      mx_leave_mc (int sock, char *groupaddr);
extern int      mx_listen (int lport, int domain, int *sock);
extern char *   mx_localhost (char *name, int len);
extern int      mx_multicast (int sock, void *buffer, int len, int port);
extern int      mx_ntohl (int nr);
extern short    mx_ntohs (short nr);
extern int      mx_reject (int sock);
extern int      mx_read (int sock, void *buffer, int *len, int *bytesleft);
extern int      mx_read_tb (int sock, tb_trac_t **trac);
extern int      mx_read_dg (int sock, void *buffer, int *len, mx_addr_t *addr);
extern int      mx_reset_sync (int sock);
extern int      mx_resume (int sock);
extern int      mx_secure_key (int sock, char *key);
extern int      mx_set_callback (int sock, int events, void (*cbfunc) (mx_evt_desc_t *, tb_trac_t *));
extern int      mx_set_sync (int sock);
extern int      mx_sockopt (int sock, int option, int optval);
extern int      mx_soundout (char *service, char *hostaddr);
extern int      mx_suspend (int sock);
extern int      mx_sync_event (mx_evt_desc_t *edesc, int timeout);
extern int      mx_write (int sock, void *buffer, int len);
extern int      mx_write_tb (int sock, tb_trac_t *trac);
extern int      mx_write_dg (int sock, void *buffer, int len, mx_addr_t *addr);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // __ILN_MX_H__
