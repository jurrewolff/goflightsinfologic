package c_mx

/*
#include "ilnmx.h"

#cgo CFLAGS: -I${SRCDIR}
#cgo LDFLAGS: -L${SRCDIR}/clib -lmx -ltb++
*/
import "C"

func MXInit(maxSocks, sndBufSize, rcvBufSize, exitHandle int) int {
	r := C.mx_init(C.int(maxSocks), C.int(sndBufSize), C.int(rcvBufSize), C.int(exitHandle))
	return int(r)
}
