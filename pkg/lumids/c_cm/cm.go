package c_cm

/*
#include <stdlib.h>
#include "ilncm.h"

void cm_message_patched(char *fmt) {
	return cm_message(fmt);
}

#cgo CFLAGS: -I${SRCDIR}
#cgo LDFLAGS: -L${SRCDIR}/clib -lcm -lini
*/
import "C"
import (
	"fmt"
	"sync"
	"unsafe"
)

type cmSession struct {
	registered bool
}

var cmSessionInstance *cmSession
var once sync.Once

// GetCMSession returns *cmSession singleton
func GetCMSession() *cmSession {
	once.Do(func() { cmSessionInstance = &cmSession{} })

	return cmSessionInstance
}

func (s *cmSession) Register(version string, mxResp int) error {
	if s.registered {
		return fmt.Errorf("already registered at cm")
	}

	cMXResp := C.int(mxResp)
	cmResp := int(C.cm_register(C.CString(version), &cMXResp))
	if cmResp != 0 {
		err := getErrStr(cmResp)
		return fmt.Errorf("cm registration failed: %s", err)
	}

	s.registered = true
	return nil
}

func (s *cmSession) Deregister() {
	C.cm_deregister()
	s.registered = false
}

func (s *cmSession) CantRun(msg string) {
	C.cm_cant_run(C.CString(msg))
}

func (s *cmSession) Message(msg string) {
	C.cm_message_patched(C.CString(msg))
}

func (s *cmSession) GetStringProperty(section, property string) string {
	cSec := C.CString(section)
	cProp := C.CString(property)
	defer C.free(unsafe.Pointer(cSec))
	defer C.free(unsafe.Pointer(cProp))

	ptrBuf := C.malloc(C.sizeof_char * 512)
	defer C.free(unsafe.Pointer(ptrBuf))

	C.cm_get_prop_str(cSec, cProp, (*C.char)(ptrBuf)) // Casting ptrBuf to '*C.char' type.

	return C.GoString((*C.char)(ptrBuf))
}

func (s *cmSession) IsRegistered() bool {
	return s.registered
}

func getErrStr(code int) string {
	r := C.cm_get_err_str(C.int(code))

	return C.GoString(r)
}
