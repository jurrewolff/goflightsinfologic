package c_log2

/*
#include "ilnlog2.h"

void patched_log2_message(int prio, char *fmt) {
	return log2_message(prio, fmt);
}

#cgo CFLAGS: -I${SRCDIR}
#cgo LDFLAGS: -L${SRCDIR}/clib -lcm -lmx -llog2 -ltb++
*/
import "C"
import (
	"fmt"
	"sync"
)

const (
	EMERG   = 0
	ALERT   = 1
	CRIT    = 2
	ERR     = 3
	WARNING = 4
	NOTICE  = 5
	INFO    = 6
	DEBUG   = 7
)

type cmLogger struct {
	initialized bool
}

var cmLoggerInstance *cmLogger
var once sync.Once

func GetCMLogger() *cmLogger {
	once.Do(func() { cmLoggerInstance = &cmLogger{} })

	return cmLoggerInstance
}

func (l *cmLogger) Init(id string, addUser int) error {
	cId := C.CString(id)
	cAddUser := C.int(addUser)

	r := float64(C.log2_init(cId, cAddUser))
	if r != 0 {
		return fmt.Errorf("cm logger initialization failed with code: %f", r)
	}

	l.initialized = true
	return nil
}

func (l *cmLogger) Close() {
	C.log2_close()
	l.initialized = false
}

func (l *cmLogger) Message(priority int, msg string) {
	C.patched_log2_message(C.int(priority), C.CString(msg))
}

func (l *cmLogger) IsInitialized() bool {
	return l.initialized
}
