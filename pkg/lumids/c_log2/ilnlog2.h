/*!----------------------------------------------------------------------------
 * @file ilnlog2.h
 * @brief Header file for the Log 2 logging system API.
 *
 * This contains the prototypes and constants for programs that use the ILNLOG2 API.
 *
 * @author R. Davis
 * @bug No known bugs.
 *
 * @copyright 2018 through 2019 Infologic Nederland B.V. All rights reserved.
 *
 * @version 1.0.0 Initial version.
 *
 *------------------------------------------------------------------------------
 */

#ifndef __ILN_LOG2_H__
#define __ILN_LOG2_H__

/*------------------------------------------------------------------------------
 * includes
 *------------------------------------------------------------------------------
 */

#include <syslog.h>

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

/*------------------------------------------------------------------------------
 * general definitions
 *------------------------------------------------------------------------------
 */

// LOG2 log levels/categories for messages - just to cover syslog use

#define LOG2_LVL_DEBUG      LOG_DEBUG                   /*!< Debug level                */
#define LOG2_LVL_INFO       LOG_INFO                    /*!< Info level                 */
#define LOG2_LVL_NOTICE     LOG_NOTICE                  /*!< Notice level               */
#define LOG2_LVL_WARNING    LOG_WARNING                 /*!< Warning level              */
#define LOG2_LVL_ERROR      LOG_ERR                     /*!< Error level                */
#define LOG2_LVL_CRIT       LOG_CRIT                    /*!< Critical level             */
#define LOG2_LVL_ALERT      LOG_ALERT                   /*!< Alert level                */

// LOG2 variadic macros for each level/category

#define LOG2_DEBUG(...)     log2_message (LOG2_LVL_DEBUG, __VA_ARGS__)
#define LOG2_INFO(...)      log2_message (LOG2_LVL_INFO, __VA_ARGS__)
#define LOG2_NOTICE(...)    log2_message (LOG2_LVL_NOTICE, __VA_ARGS__)
#define LOG2_WARNING(...)   log2_message (LOG2_LVL_WARNING, __VA_ARGS__)
#define LOG2_ERROR(...)     log2_message (LOG2_LVL_ERROR, __VA_ARGS__)
#define LOG2_CRIT(...)      log2_message (LOG2_LVL_CRIT, __VA_ARGS__)
#define LOG2_ALERT(...)     log2_message (LOG2_LVL_ALERT, __VA_ARGS__)

/*------------------------------------------------------------------------------
 * forward declarations
 *------------------------------------------------------------------------------
 */


/*!----------------------------------------------------------------------------
 * @brief Initialise the logging 2 system.
 *
 * It is assumed that the Client has already performed an 'mx_init' and has
 * registered with ILNCM.
 *
 * @see log2_close ()
 *
 * @param id identification for logging, or NULL or empty to use CM process name
 * @param add_user add the user name to the id if true, otherwise not
 * @return error code (zero if OK) - currently only EINVAL if an error occurs
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern long     log2_init (char *id, int add_user);

/*!----------------------------------------------------------------------------
 * @brief Close the logging 2 system.
 *
 * @see log2_init ()
 *
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern void     log2_close (void);

/*!----------------------------------------------------------------------------
 * @brief Log a message
 *
 * @param prio level/priority for the message (should be one of the LOG2_LVL_...)
 * @param fmt format string (as for printf)
 * @param ... arguments as required by fmt (as for printf)
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern void     log2_message (int prio, char *fmt, ...);

/*!----------------------------------------------------------------------------
 * @brief Set the 'basic' log level
 *
 * @param level the 'basic' level to set - should be "debug", "info" or "default"
 * @since 1.0.0
 *
 *------------------------------------------------------------------------------
 */

extern void     log2_set_level (char *level);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // __ILN_LOG2_H__
