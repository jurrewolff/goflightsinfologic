package model

import (
	"database/sql"
	"errors"
	"fmt"
	"goflights/pkg/config"
	"goflights/pkg/lumids/c_log2"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

var cmLogger = c_log2.GetCMLogger()
var c = config.GetConfig()

type Arrival struct {
	RowID             int    `db:"ROW_ID"`
	Flightnumber      string `db:"FLTNR"`
	Origin            string `db:"ORIGIN"` // TODO Missing in 'lumids-dev' environment as opposed to 'lumids-test'
	SchedUnxTimestamp string `db:"SDT"`
	SchedDate         string `db:"SDATE"`
	SchedTime         string `db:"STIME"`
}

var db *sqlx.DB

func InitDB() error {
	dataSourceName := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s",
		c.Database.User,
		c.Database.Password,
		c.Database.Host,
		c.Database.Port,
		c.Database.Name,
	)

	var err error
	db, err = sqlx.Connect("mysql", dataSourceName)
	if err != nil {
		return fmt.Errorf("error connecting to database with dsn '%s': %s", dataSourceName, err)
	} else {
		cmLogger.Message(
			c_log2.DEBUG,
			fmt.Sprintf("connected to db with dsn: %s", dataSourceName),
		)
	}

	return nil
}

func SelectArrivals() ([]Arrival, error) {
	var arrvs []Arrival

	rows, err := db.Queryx("SELECT ROW_ID, FLTNR, ORIGIN, SDT, SDATE, STIME FROM ARRF")
	if err != nil {
		return []Arrival{}, fmt.Errorf("error querying db: %s", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			cmLogger.Message(c_log2.ERR, fmt.Sprintf("error closing query rows: %s", err))
		}
	}()

	for rows.Next() {
		var a Arrival
		err = rows.StructScan(&a)
		if err != nil {
			return []Arrival{}, fmt.Errorf("error scanning row into struct: %s", err)
		}

		arrvs = append(arrvs, a)
	}

	return arrvs, nil
}

func SelectArrival(a Arrival) (Arrival, error) {
	var q = "SELECT ROW_ID, FLTNR, ORIGIN, SDT, SDATE, STIME FROM ARRF WHERE ROW_ID = :ROW_ID"
	namedStmt, err := db.PrepareNamed(q)
	if err != nil {
		return Arrival{}, fmt.Errorf("error preparing named statement: %s", err)
	}

	row := namedStmt.QueryRowx(a)
	err = row.StructScan(&a)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return Arrival{}, nil
		}
		return Arrival{}, fmt.Errorf("error scanning row into struct: %s", err)
	}

	return a, nil
}

func InsertArrival(a Arrival) error {
	var err error

	q := "INSERT INTO ARRF (ROW_ID, FLTNR, ORIGIN, SDT, STIME, SDATE) VALUES (:ROW_ID, :FLTNR, :ORIGIN, :SDT, :STIME, :SDATE)"
	namedStmt, err := db.PrepareNamed(q)
	if err != nil {
		return fmt.Errorf("error preparing named statement: %s", err)
	}

	_, err = namedStmt.Exec(a)
	if err != nil {
		return fmt.Errorf("error inserting row with id '%d' into table 'ARRF': %s", a.RowID, err)
	}

	return nil
}

func UpdateArrival(a Arrival) error {
	var q = "UPDATE ARRF SET ORIGIN = :ORIGIN, FLTNR = :FLTNR WHERE ROW_ID = :ROW_ID"
	namedStmt, err := db.PrepareNamed(q)
	if err != nil {
		return fmt.Errorf("error preparing named statement: %s", err)
	}

	_, err = namedStmt.Exec(a)
	if err != nil {
		return fmt.Errorf("error updating row with id '%d': %s", a.RowID, err)
	}
	return nil
}

func DeleteArrival(a Arrival) error {
	var q = "DELETE FROM ARRF WHERE ROW_ID = :ROW_ID"
	namedStmt, err := db.PrepareNamed(q)
	if err != nil {
		return fmt.Errorf("error preparing named statement: %s", err)
	}

	_, err = namedStmt.Exec(a)
	if err != nil {
		return fmt.Errorf("error deleting  row with id '%d': %s", a.RowID, err)
	}
	return nil
}

func GetTableId(tableName string) (int, error) {
	var tableId int
	var q = fmt.Sprintf("SELECT DD_TABLE_NUMBER FROM DD_TABLES WHERE DD_TABLE_NAME = '%s'", tableName)

	err := db.Get(&tableId, q)
	if err != nil {
		return tableId, fmt.Errorf("error getting result from db: %w", err)
	}

	return tableId, nil
}
