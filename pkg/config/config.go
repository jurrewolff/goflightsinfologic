package config

import (
	"fmt"
	"goflights/pkg/lumids/c_cm"
	"strconv"
	"sync"

	"github.com/kelseyhightower/envconfig"
)

var cmSession = c_cm.GetCMSession()

var cfgInstance *config
var once sync.Once

func GetConfig() *config {
	once.Do(func() { cfgInstance = &config{} })

	return cfgInstance
}

type config struct {
	Webserver struct {
		Address string `envconfig:"WEBSERVER_ADDRESS"`
		Port    int    `envconfig:"WEBSERVER_PORT"`
	}
	Database struct {
		User     string `envconfig:"DB_USER"`
		Password string `envconfig:"DB_PASSWORD"`
		Host     string `envconfig:"DB_HOST"`
		Port     int    `envconfig:"DB_PORT"`
		Name     string `envconfig:"DB_NAME"`
	}
	BaseConfig struct {
		ApiType string `envconfig:"API_TYPE"`
	}
}

func (c *config) Load() error {
	err := c.loadCfgCM()
	if err != nil {
		return fmt.Errorf("error reading CM config: %s", err)
	}

	err = c.loadCfgEnv()
	if err != nil {
		return fmt.Errorf("error reading environment config: %s", err)
	}

	return nil
}

func (c *config) loadCfgEnv() error {
	err := envconfig.Process("", c)
	if err != nil {
		return fmt.Errorf("error processing environment variables into config: %s", err)
	}
	return nil
}

// loadCfgCM loads configuration held by CM
func (c *config) loadCfgCM() error {
	var err error

	if !cmSession.IsRegistered() {
		return fmt.Errorf("can't load cm config while not registered at cm")
	}

	// Webserver
	webSrvAddr := cmSession.GetStringProperty("GoFlights", "webserver_address")
	webSrvPort := cmSession.GetStringProperty("GoFlights", "webserver_port")
	c.Webserver.Address = webSrvAddr
	if webSrvPort != "" {
		c.Webserver.Port, err = strconv.Atoi(webSrvPort)
		if err != nil {
			return fmt.Errorf("error converting webserver_port property to integer: %s", err)
		}
	}

	// Database
	c.Database.Host = cmSession.GetStringProperty("RDBParams", "hostname")
	c.Database.Name = cmSession.GetStringProperty("RDBParams", "databasename")
	c.Database.User = cmSession.GetStringProperty("RDBParams", "username")
	c.Database.Password = cmSession.GetStringProperty("RDBParams", "password")
	dbPort := cmSession.GetStringProperty("RDBParams", "databaseport")
	if dbPort != "" {
		c.Database.Port, err = strconv.Atoi(dbPort)
		if err != nil {
			return fmt.Errorf("error converting webserver_port property to integer: %s", err)
		}
	}

	// Base config
	c.BaseConfig.ApiType = cmSession.GetStringProperty("GoFlights", "api_type")

	return nil
}
