# goflights
The "goflights" project serves as a basis for developing tailor made Lumids modules written in Go. The product is the
result of Jurre Wolff's graduation internship.

# Description
The goflights base module was developed to speed up the realization of Lumids tailor made communication modules. The
base module is a fully fledged Lumids module written in Go, in the form of a working SOAP API. The API serves as an
example implementation with code for functionalities such as database, Configuration Manager-, Row Server- and
Distribute Server interaction.

The base module provides a modular architecture, allowing easy adaptation and extension to fit any custom project.

# Usage
## Project setup
To start the development of a new custom module, the base module must first be cloned into a directory of your choice:
```bash
mkdir /path/to/custom_project
cd /path/to/custom_project
git clone ssh://git@gitlab.infologic.nl:2222/jwolff/goflights.git
```

After this, a new GitLab project can be created, based on the master branch of the base module. Make sure to substitute
the "username" and "projectname":
```bash
cd ./goflights
git push --set-upstream ssh://git@gitlab.infologic.nl:2222/username/projectname.git master
```

Afterwards, delete the original project:
```bash
cd ../
rm -rf ./goflights
```

Navigate to the GitLab website to configure the correct project settings and then clone the created project. Make sure
to enter the correct "username" and "projectname" you chose earlier:
```bash
git clone ssh://git@gitlab.infologic.nl:2222/username/projectname.git
```

A fresh Git project is ready for development. The next paragraph will discuss how to use the Go modules system.

## Build & Deploy
After the project has been set up, development can begin. During this period the application will have to be built and
deployed for testing. During the build process the Lumids libraries (64- OR 32-bit) are required to be present. If the
Lumids libraries are not stored in a standard library directory, the following command can be used to manually set a
path:
```bash
export LD_LIBRARY_PATH=/path/to/libraries/
```

To build the application to a binary file, the following command can then be executed. Note that the module name has to
be replaced with the name of your custom module:
```bash
CGO_ENABLED=1 GOOS=linux go build -o modulenaam cmd/goflights/main.go
```

The environment variable 'CGO_ENABLED' is needed to activate cgo allowing for integration with the C libraries. In
addition, the 'GOOS' variable explicitly specifies that the application is built for Linux.

If the project structure changes, the command must be updated to reflect the path to the main package.

The resulting file is a module that can be copied into the Lumids environment. Please note that the “user” and “ip” must
be corresponding with the Lumids environment configuration:
```bash
scp /path/to/modulename user@ip:/opt/infologic/lumids/bin
```

If the module is configured correctly in the Lumids environment (Configuration of Go modules is the same as C modules)
the module can be started like any other:
```bash
lumidsctl start modulename
```
