package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"goflights/pkg/config"
	"goflights/pkg/lumids/c_cm"
	"goflights/pkg/lumids/c_ds"
	"goflights/pkg/lumids/c_log2"
	"goflights/pkg/lumids/c_mx"
	"goflights/pkg/lumids/c_rs"
	"goflights/pkg/model"
	"goflights/pkg/route"
	"goflights/pkg/version"
	"log"
	"net/http"
)

var cmLogger = c_log2.GetCMLogger()
var cmSession = c_cm.GetCMSession()
var rsSession = c_rs.GetRSSession()
var dsSession = c_ds.GetDSSession()

var c = config.GetConfig()

func handleError(err error) {
	errStr := fmt.Sprintf("%s", err)

	if cmLogger.IsInitialized() {
		cmLogger.Message(c_log2.ERR, errStr)
	}

	if cmSession.IsRegistered() {
		cmSession.CantRun(errStr)
	}

	log.Fatal(errStr)
}

func main() {
	mxResp := c_mx.MXInit(256, 0, 0, 1)
	err := cmSession.Register(version.APPVERSION, mxResp)
	if err != nil {
		handleError(fmt.Errorf("error registering at cm: %s", err))
	}
	defer cmSession.Deregister()

	err = cmLogger.Init("", 0)
	if err != nil {
		handleError(fmt.Errorf("error initializing logger: %s", err))
	}
	defer cmLogger.Close()

	err = c.Load()
	if err != nil {
		handleError(fmt.Errorf("error loading config: %s", err))
	}

	err = model.InitDB()
	if err != nil {
		handleError(fmt.Errorf("error initializing db: %s", err))
	}

	err = rsSession.Init()
	if err != nil {
		handleError(fmt.Errorf("error initializing rowserver session: %s", err))
	}
	defer rsSession.Close()

	err = dsSession.Init()
	if err != nil {
		handleError(fmt.Errorf("error initializing distribute server session: %w", err))
	}
	defer dsSession.Close()

	r, err := route.SetRoutes()
	if err != nil {
		handleError(fmt.Errorf("error setting routes: %s", err))
	}

	err = serve(c.Webserver.Address, c.Webserver.Port, r)
	if err != nil {
		handleError(fmt.Errorf("error starting server: %s", err))
	}
}

func serve(address string, port int, r *mux.Router) error {
	serverAddress := fmt.Sprintf("%s:%d", address, port)
	msg := fmt.Sprintf("Go SOAP API listening on '%s'", serverAddress)
	log.Println(msg)
	cmLogger.Message(c_log2.INFO, msg)

	err := http.ListenAndServe(serverAddress, r)
	if err != nil {
		return err
	}

	return nil
}
